<?php
include('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Hostal Rosita </title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Resort Inn Responsive , Smartphone Compatible web template , Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen">
<link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
<link rel="stylesheet" href="css/jquery-ui.css" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Federo" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<!--//fonts-->
</head>
<body>
<!-- header -->
	<div class="w3_navigation">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Palanca de navegacion</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<h1><a class="navbar-brand" href="index.php"> HOsTAL <span>ROSITA</span><p class="logo_w3l_agile_caption">Un  Lugar como en casa </p></a></h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--iris">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item"><a href="index.php" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="habitaciones.php" class="menu__link">Habitaciones</a></li>
							<li class="menu__item"><a href="barkaraoke.php" class="menu__link">Bar Karaoke</a></li>
							<li class="menu__item menu__item--current"><a href="actividades.php" class="menu__link">Actividades</a></li>
							<li class="menu__item"><a href="galeria.php" class="menu__link">Galeria</a></li>
							<li class="menu__item"><a href="contactos.php" class="menu__link">Contactos</a></li>
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>
<!-- //header -->
<!-- team -->
<div class="team" id="team">
	<div class="container">
		<h3 class="title-w3-agileits title-black-wthree">ACTIVIDADES</h3>
		<div class="row text-center">
			<div class="col-lg-4" >
				<h2>Volcán Cotopaxi</h2>
				<h2>Cotopaxi</h2>
				<h2>Ecuador</h2>
				<img src="images/parque-nacional-cotopaxi.jpg" alt=" " style="width:100%; height: 200px"/>
			</div>
			<div class="col-lg-4">
				<h2>El Boliche</h2>
				<h2>Cotopaxi </h2>
				<h2>Ecuador </h2>
				<img src="images/el_boliche.jpg" alt=" " style="width:100%; height: 200px"/>
			</div>
			<div class="col-lg-4">
				<h2>La Mama Negra</h2>
				<h2>Celebración </h2>
				<h2>Ecuador</h2>
				<img src="images/mama_negra.jpg" alt=" " style="width:100%; height: 200px"/>
			</div>
		</div><br>
		<div class="row text-center" >
			<div class="col-lg-6">
				<h2>El Quilotoa</h2>
				<h2>Laguna</h2>
				<h2>Ecuador</h2>
				<img src="images/Quilotoa.png" alt=" " style="width:70%; height: 200px"/>
			</div>
			<div class="col-lg-6">
				<h2>Náutico Ignacio Flores</h2>
				<h2>Parque</h2>
				<h2>Ecuador</h2>
				<img src="images/204_lalaguna.jpg" alt=" " style="width:70%; height: 200px"/>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="tab1">
					<div class="col-md-6">
						<img src="images/parque-nacional-cotopaxi.jpg" alt="" style="width:100%">
					</div>
					<div class="col-md-6 team-Info-agileits">
						<h4>VOLCÁN COTOPAXI</h4>
						<span>El nombre de la montaña es una voz Cayapa que se descompone así: Coto, cuello; pag, de pagta, sol y si de shi, dulce. Es decir, "Dulce Cuello de Sol".</span>
						<p>El volcán Cotopaxi con una elevación de 5.897 metros es el segundo de más altura del país y uno de los volcanes activos más altos del mundo. Las últimas erupciones registradas fueron en 1877 y 1904, y tuvo alguna actividad en 1942. Actualmente se registran incrementos en su actividad volcánica desde el año 2003; siendo éste el año con mayor actividad reportada.</p>					
					</div> 
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="tab1">
					<div class="col-md-6">
						<img src="images/el_boliche.jpg" alt="" style="width:100%">
					</div>
					<div class="col-md-6 team-Info-agileits">
						<h4>El Boliche</h4>
						<span>Se encuentra ubicado junto al Parque Nacional Cotopaxi, en la parroquia de Mulalo, a 27 km. de Machachi y 44 km. de Latacunga</span>
						<p>El Boliche, posee características ecológicas, biológicas y paisajísticas sobresalientes; aquí la grandiosidad de la naturaleza es manejada para beneficio de la sociedad ecuatoriana y mundial.</p>					
					
					</div> 
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="tab1">
					<div class="col-md-6">
						<img src="images/mama_negra.jpg" alt="" style="width:100%">
					</div>
					<div class="col-md-6 team-Info-agileits">
						<h4>Mama Negra</h4>
						<span>La Mama Negra es una fiesta popular ecuatoriana de la Sierra propiamente de la ciudad de Latacunga es muy conocida dentro del país y fuera de el . Su origen es una muestra de las manifestaciones populares mestizas históricas que impregnan toda su historia y folklore a través de esta celebración. Esta celebración fue creada por Rafael Sandoval Pastor, con actualmente 95 años.</span>
						<p>Esta fiesta se lleva a cabo a finales de septiembre, los días 23 y 24, días en los que la Iglesia Católica conmemora a la Virgen de la Merced. Esta celebración se ha convertido a lo largo de los años en una de las festividades de mayor importancia en el país. Ésta es una celebración que consiste en un desfile de varios personajes pintorescos que realizan una comparsa por las calles de Latacunga, en donde la Mama Negra es el personaje principal, y representa a la Virgen María, la cual pasa por las calles cabalgando con una vestimenta muy peculiar: camisa bordada, adornos múltiples y pañolones largos sobresalen en el conjunto de su vestimenta. Lleva una muñeca a la que hace bailar, pero lo más característico durante esta comparsa es el recipiente lleno de leche y agua que hacer caer sobre los espectadores de personajes como el Rey Moro, el Ángel de las Estrellas y los Huacos, que también son parte de esta comparsa, representando cada uno parte de la historia precolombina de la región. Esta celebración convoca a turistas nacionales y extranjeros.</p>					
					
						</div> 
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="tab1">
					<div class="col-md-6">
						<img src="images/Quilotoa.png" alt="" style="width:100%">
					</div>
					<div class="col-md-6 team-Info-agileits">
						<h4>El Quilotoa</h4>
						<span>Se permite acampar en el fondo del cráter, aunque no hay agua potable (excepto por las botellas de medio litro vendidas en los hostales), sin embargo existen baños con servicios higiénicos y urinarios tanto para hombres como para mujeres.</span>
						<p>El Volcán Quilotoa (3.900 msnm), localizado en la Provincia de Cotopaxi, Parroquia de Zumbahua, forma parte de la Reserva Ecológica Los Iliniza. Su nombre proviene de dos vocablos quichuas "quiru" que quiere decir diente y "toa" que significa reina debido a la forma de la laguna, pues ésta tiene forma casi elíptica de aproximadamente 3.15 kilómetros de diámetro y una diferencia de 440 metros entre el nivel del agua y el borde superior. El borde del cráter remata en el lado suroeste con la cumbre Huyan tic o Puerta Zhalaló que tiene 4.010 msnm.</p>					
					</div> 
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="tab1">
					<div class="col-md-6">
						<img src="images/204_lalaguna.jpg" alt="" style="width:100%">
					</div>
					<div class="col-md-6 team-Info-agileits">
						<h4>Parque náutico Ignacio Flores</h4>
						<span>Latacunga, Provincia de Cotopaxi, Ecuador</span>
						<p>En honor al latacungueño Ingacio Flores conocido como el “pacificador del Peru”</p>					
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="copy">
		<p>© 2018  <a href="index.php">AMENECER</a> </p>
	</div>
	<!--/footer -->
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<!-- contact form -->
	<script src="js/jqBootstrapValidation.js"></script>

<!-- /contact form -->	
<!-- Calendar -->
		<script src="js/jquery-ui.js"></script>
		<script>
				$(function() {
				$( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
				});
		</script>
<!-- //Calendar -->
<!-- gallery popup -->
<link rel="stylesheet" href="css/swipebox.css">
				<script src="js/jquery.swipebox.min.js"></script> 
					<script type="text/javascript">
						jQuery(function($) {
							$(".swipebox").swipebox();
						});
					</script>
<!-- //gallery popup -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- flexSlider -->
				<script defer src="js/jquery.flexslider.js"></script>
				<script type="text/javascript">
				$(window).load(function(){
				  $('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				  });
				});
			  </script>
			<!-- //flexSlider -->
<script src="js/responsiveslides.min.js"></script>
			<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider4").responsiveSlides({
							auto: true,
							pager:true,
							nav:false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							  $('.events').append("<li>before event fired.</li>");
							},
							after: function () {
							  $('.events').append("<li>after event fired.</li>");
							}
						  });
					
						});
			</script>
		<!--search-bar-->
		<script src="js/main.js"></script>	
<!--//search-bar-->
<!--tabs-->
<script src="js/easy-responsive-tabs.js"></script>
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script>
<!--//tabs-->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	
	<div class="arr-w3ls">
	<a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
<!-- //smooth scrolling -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
</body>
</html>


